package exam;

import exam.core.*;

import java.io.Serializable;
import java.util.*;

public class CityN implements Serializable {

    private Set<Citizen> citizens;
    public static Map<Integer, Street> streets = new LinkedHashMap<Integer, Street>() {{
        put(0, new Street(0, "0"));
        put(1, new Street(1, "1"));
        put(2, new Street(2, "2"));
        put(3, new Street(3, "3"));
        put(4, new Street(4, "4"));
    }};

//Test constructor
//    public CityN() {
//        citizens = new HashSet<>();
//        for (int i = 0; i < 1_500; i++) {
//            citizens.add(new Citizen(i, i + 2, i + 3, i + 4, "t" + i, "ln" + i, "fn" + i, "sn" + i, "1", LocalDateTime.now(), "1", new Address("street" + i, "house" + i, "hl" + i, "corp" + i, "fl" + i, "flLit" + i), "pn" + i, "ps" + i, i, new Organization(), LocalDateTime.now(), LocalDateTime.now()));
//        }
//    }

    public CityN(){
        citizens = new HashSet<>();
    }

    //TODO: method for filter values in citizens
    public List filterBy(String property, String expr) {
        return null;
    }

    //TODO method for sort values
    public List getSortedCitizensBy(String... properties) {

        return null;
    }

    //TODO method for request
    public List getCitizensByRequest() {

        return null;
    }

    public void addCitizen(Citizen citizen) {
        citizens.add(citizen);
    }

    public Set<Citizen> getCitizens() {
        return citizens;
    }

    public int getCitizenQty() {
        return citizens.size();
    }

    public static Map<Integer, Street> getStreets() {
        return streets;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CityN{");
        sb.append("citizens=").append(citizens);
        sb.append('}');
        return sb.toString();
    }

    //updated
    public ArrayList<Citizen> getCitizenByName(String firstName, String secondName, String lastName)
    {
        ArrayList<Citizen> list = new ArrayList<>();

        String name = firstName.toUpperCase() + " " + secondName.toUpperCase() + " " + lastName.toUpperCase();
        for(Citizen c : citizens)
        {
            if(isContains(name,c.getName()));
        }
        return list;
    }

    public ArrayList<Citizen> getCitizensByDate(Calendar date)
    {
        ArrayList<Citizen> list = null;
        for(Citizen c : citizens)
        {
            if(date.equals(c.LdtBirthday())) list.add(c);
        }
        return list;
    }

    public ArrayList<Citizen> getCitizensByRegion(Region region)
    {
        ArrayList<Citizen> list = null;
        for(Citizen c : citizens)
        {
            if(region.equals(c.Region())) list.add(c);
        }
        return list;
    }

    public ArrayList<Citizen> getCitizenByAdress(Address address)
    {
        ArrayList<Citizen> list = null;
        for(Citizen c : citizens)
        {
            if(address.equals(c.getAddress())) list.add(c);
        }
        return list;
    }

    public ArrayList<Citizen> getCitizenByNoDocument(Integer number)
    {
        ArrayList<Citizen> list = null;
        for(Citizen c : citizens)
        {
            if(((number & c.getDocNumber())==1)){
            list.add(c);
        }
        }
        return list;
    }

    boolean isContains(String str1, String str2)
    {
        try
        {
            str2 = str2.toUpperCase();
            return str1.contains(str2);
        }
        catch(Exception e)
        {
            return false;
        }
    }
}
