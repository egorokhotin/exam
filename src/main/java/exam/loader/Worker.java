package exam.loader;

import exam.CityN;
import exam.core.*;

import java.io.*;
import java.util.*;

public class Worker {

    private static String pathToDataBases;

    //TODO
    //Принимает строку (название файла) и возвращает (List/Массив строк) с (названиями файлов/полные пути до файлов)
    //getFileList("ins") вернет {../ins.txt , ./ins1.txt }
    //(проверять в любом регистре, а возвращать в оригинальном)
    public static List<String> getFileList(String fileName){
        return null;
    }


    public static void setPathToDataBases(String pathToDataBases) {
        Worker.pathToDataBases = pathToDataBases;
    }

    public static CityN importDB(String path) {

        return null;
    }

    public static void loadDB(){
        loadDataBase(0, "STREETS.txt", "streets1.txt");
        loadDataBase(1, "STANDART.txt", "standart1.txt");
        loadDataBase(2, "pred.txt", "pred.txt");
        loadDataBase(3, "ins.txt"/*, "ins1.txt"*/);
        //Файл второй файл не парсится из-за ковычек и другого рзделительного знака
        //Исправить. Таск для Егора, хотя писал это я. Посмотрим
    }

    // Один метод для всех баз данных. В параметры передаем путь до папки с БД, тип БД,
    // имена файлов, содержащих данные.
    // baseType: 0 - улицы, 1 - регионы, 2 - организации, 3 - горожане.
    private static void loadDataBase(int baseType, String... fileNames) {
        if (fileNames.length != 0) {
            BufferedReader bufferedReader = null;
            String line = " ";
            for (String x : fileNames) {
                try {
                    bufferedReader = getReader(pathToDataBases + "/" + x);
                } catch (Exception e) {
                    System.err.println("Can't open file");
                }
                try {
                    bufferedReader.readLine();
                    //метка dsds для прерывания добавления пользователей при достижении определенного кол-ва (см. case 3)
                    dsds:
                    while (line.length()!=0) {
                        line = bufferedReader.readLine();
                        try {
                            switch (baseType) {
                                case 0: {
                                    addStreet(line);
                                    break;
                                }
                                case 1: {
                                    addRegion(line);
                                    break;
                                }
                                case 2: {
                                    addOrganization(line);
                                    break;
                                }
                                case 3: {
                                    addCitizen(line);
                                    if (Data.citizenQty() > 1000) {
                                        break dsds;
                                    }
                                    break;
                                }
                                default: {
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    //Incorrect line format. Ignore.
                }
                finally {
                    try{bufferedReader.close();} catch (IOException e){e.printStackTrace();}
                }
            }
        }
    }

    private static BufferedReader getReader(String path) {
        try {
            FileInputStream inputStream = new FileInputStream(path);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            return bufferedReader;
        } catch (FileNotFoundException e) {
            System.err.println("Error with getting Reader: path=\"" + path);
        }
        return null;
    }

    private static void addStreet(String line) {
        Data.addStreet(line);
    }


    private static void addRegion(String line) {
        Data.addRegion(line);
    }

    private static void addOrganization(String line) {
        Data.addOrganisation(line);
    }

    //TODO
    //Парсить без создания String[].
    private static void addCitizen(String string) {
        String[] strings = string.split("\t");
        Citizen c = new Citizen();
        int i =-1;
        c.setId(strings[++i]);
        c.setDocNumber(Integer.parseInt(strings[++i]));
        c.setDocNSeries(Integer.parseInt(strings[++i]));
        c.setDocSeries(strings[++i]);
        c.setDocType(strings[++i]);
        c.setLastName(strings[++i]);
        c.setFirstName(strings[++i]);
        c.setSecondName(strings[++i]);
        c.setStatus(strings[++i]);
        c.setBirthDay(strings[++i]);
        c.setSex(strings[++i]);
        c.setPolicyNumber(strings[++i]);
        c.setPolicySeries(strings[++i]);
        c.setRegion(Data.findRegionById(Integer.parseInt(strings[++i]), //RGN1
                Integer.parseInt(strings[++i]),                         //RGN2
                Integer.parseInt(strings[++i])));                       //RGN3
        c.setAddress(new Address(Data.findStreetById(Integer.parseInt(strings[++i])), // Street
                strings[++i],   //house
                strings[++i],   //houseLiter
                strings[++i],   //corpus
                strings[++i],   //flat
                strings[++i])); //flatLiter
        c.setLocale(Integer.parseInt(strings[++i]));
        i+=5;
        c.setTin(Long.parseLong(strings[i]));
        c.setOrganization(Data.findOrganizationByInn(Long.parseLong(strings[i++])));
        c.setDateIn(strings[++i]);
        c.setChangeDate(strings[++i]);
        Data.cityN.addCitizen(c);
        /*Data.cityN.addCitizen(new Citizen(strings[0], //id
                Integer.parseInt(strings[1]),         //docNumber
                Integer.parseInt(strings[2]),         //docNSeries
                strings[3],                           //docSeries
                strings[4],                           //docType
                strings[5],                           //lastName
                strings[6],                           //firstName
                strings[7],                           //secondName
                strings[8],                           //status
                strings[9],                           //birthDay
                strings[10],                          //sex
                strings[11],                          //policyNumber
                strings[12],                          //policySeries
                Data.findRegionById(Integer.parseInt(strings[13]),
                        Integer.parseInt(strings[14]),
                        Integer.parseInt(strings[15])),//setRegion
                new Address(Data.findStreetById(Integer.parseInt(strings[16])),
                        strings[17],
                        strings[18],
                        strings[19],
                        strings[20],
                        strings[21]),                 //setAddress
                Integer.parseInt(strings[22]),        //locale
                Long.parseLong(strings[27]),          //tin
                Data.findOrganizationByInn(Long.parseLong(strings[27])),
                strings[29],
                strings[30]
        ));*/
    }

    //    private static void loadStreets(String path, String... fileNames)
//    {
//        try {
//            BufferedReader bufferedReader = getReader(path);
//            bufferedReader.readLine();
//            String[] strings = bufferedReader.readLine().split("\\t");
//            while(strings.length != 0)
//            {
//                addStreet(strings, Data.streets);
//                strings = bufferedReader.readLine().split("\\t");
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        catch(Exception ignored)
//        {
//
//        }
//    }

//    private static void addOrganization(String[] strings, SimpleDateFormat format, Set<Organization> list)
//    {
//        try {
//            long inn = Long.parseLong(strings[0]);
//            int insurer = Integer.parseInt(strings[1]);
//            String name = strings[4];
//            int t = 5;
//            int arg_num = Integer.parseInt(strings[t++]);
//            Organization org = new Organization(inn, insurer, arg_num);
//            org.setName(name);
//            org.setDate(format.parse(strings[t]));
//            t += 3;
//            int rgn1 = Integer.parseInt(strings[t++]);
//            int rgn2 = Integer.parseInt(strings[t++]);
//            int rgn3 = Integer.parseInt(strings[t++]);
//            Region rg = Data.findRegionById(rgn1, rgn2, rgn3);
//            org.setRegion(rg);
//            Street street = Data.findStreetById(Integer.parseInt(strings[t++]));
//            Address addrr = new Address(street, strings[t], strings[t + 1], strings[t + 2], strings[t + 3], strings[t + 4]);
//            org.setAddress(addrr);
//            t += 5;
//            org.setPhoneNumber(Integer.parseInt(strings[t++]));
//            org.setFace(strings[t++]);
//            org.setRf(Integer.parseInt(strings[t++]));
//            org.setAdm(Integer.parseInt(strings[t++]));
//            org.setBik(Integer.parseInt(strings[t++]));
//            org.setBankName(strings[t++]);
//            org.setOkpo(strings[t++]);
//            org.setOkonh(strings[t]);
//            list.add(org);
//        }
//        catch(ParseException pEx)
//        {
//
//        }
//        catch(Exception ignored)
//        {
//
//        }
//    }

    //    private static void loadOrganization(String path)
//    {
//        try {
//            BufferedReader bufferedReader = getReader(path);
//            bufferedReader.readLine();
//            String[] strings = bufferedReader.readLine().split("\\t");
//            SimpleDateFormat format = new SimpleDateFormat("dd.mm.yyyy");
//            while (strings.length != 0) {
//                addOrganization(strings, format, Data.organizations);
//                strings = bufferedReader.readLine().split("\\t");
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        catch(Exception ignored)
//        {
//
//        }
//    }


    //    private static void loadDataBase(String path)
//    {
//        try {
//            BufferedReader bufferedReader = getReader(path);
//            bufferedReader.readLine();
//            String[] strings = bufferedReader.readLine().split("\\t");
//            int count = 0;
//            while(strings.length != 0 && count < 100)
//            {
//                addCitizen(strings);
//                count++;
//                strings = bufferedReader.readLine().split("\\t");
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        catch(Exception ignored)
//        {
//
//        }
//    }

//    private static void addStreet(String[] strings, Set<Street> list)
//    {
//        list.add(new Street(Integer.parseInt(strings[0]), strings[1]));
//    }

    //    public static void loadDB(String path){
//        try {
//            FileInputStream inputStream = new FileInputStream(path);
//            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
//            bufferedReader.readLine();
//            loadStreets(null);
//            loadRegions(null);
//            loadOrganization(null);
//            loadDataBase(null);
//            String[] strings = bufferedReader.readLine().split("\\t");
//            int count = 0;
//            while (strings.length != 0 && count != 100000){
//                try {
//
//                } catch (Exception e){
//
//                }
//                count++;
//                strings = bufferedReader.readLine().split("\\s");
//            }
//
//        } catch (FileNotFoundException e){
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        catch(Exception ignored)
//        {
//
//        }
//    }

    //
//    private static void loadRegions(String path)
//    {
//        try {
//            BufferedReader bufferedReader = getReader(path);
//            bufferedReader.readLine();
//            String[] strings = bufferedReader.readLine().split("\\t");
//            while(strings.length != 0)
//            {
//                addRegion(strings, Data.regions); //Add set
//                strings = bufferedReader.readLine().split("\\t");
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        catch(Exception ignored)
//        {
//
//        }
//    }

//    private static void addRegion(String[] strings, Set<Region> list)
//    {
//        String[] regionNumbers = strings[0].split(" ");
//        Region rg = new Region(Integer.parseInt(regionNumbers[0]),Integer.parseInt(regionNumbers[1]),Integer.parseInt(regionNumbers[2]));
//        rg.setName(strings[1]);
//        rg.setInsurer(Integer.parseInt(strings[2]));
//        rg.setLabel(Integer.parseInt(strings[3]));
//        list.add(rg);
//    }


//    private static void addCitizen(String[] strings)
//    {
//        Data.cityN.addCitizen(new Citizen(strings[0],
//                    Integer.parseInt(strings[1]),
//                    Integer.parseInt(strings[2]),
//                    strings[3],
//                    strings[4],
//                    strings[5],
//                    strings[6],
//                    strings[7],
//                    strings[8],
//                    strings[9],
//                    strings[10],
//                    strings[11],
//                    strings[12],
//                    Data.findRegionById(Integer.parseInt(strings[13]),
//                            Integer.parseInt(strings[14]),
//                            Integer.parseInt(strings[15])),
//                    new Address(Data.findStreetById(Integer.parseInt(strings[16])),
//                            strings[17],
//                            strings[18],
//                            strings[19],
//                            strings[20],
//                            strings[21]),
//                    Integer.parseInt(strings[22]),
//                    Long.parseLong(strings[27]),
//                    new Organization(Long.parseLong(strings[27]),
//                            Integer.parseInt(strings[28]),
//                            Integer.parseInt(strings[31])),
//                    strings[29],
//                    strings[30]
//            ));
//    }

}
