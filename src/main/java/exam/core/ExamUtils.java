package exam.core;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class ExamUtils {

    static Logger logger = Logger.getLogger("ExamLog");
    static FileHandler fh;
    static long start, end;

    static {
        try {
            fh = new FileHandler("exam.log",true);
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void test() {
        logger.info("My first log");
    }

    public static void printToLog(String message) {
        logger.info(message);
    }

    public static void start(String message) {
        start = System.currentTimeMillis();
        logger.info(new StringBuilder()
                .append("Start work")
                .append("Time: ")
                .append(start)
                .append(" from")
                .append(message)
                .toString());
    }

    public static void end(String message) {
        end = System.currentTimeMillis();
        logger.info(new StringBuilder()
                .append("End work")
                .append("Time: ")
                .append(end)
                .append(" from")
                .append(message)
                .append(" TOTAL TIME")
                .append(end-start)
                .toString());
        start=0;
        end=0;
    }
//    public static void

}
