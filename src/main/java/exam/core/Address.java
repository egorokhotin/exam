package exam.core;

import java.io.Serializable;

public class Address implements Serializable {

    private Street street;
    private String house;
    private String houseLiter;
    private String corpus;
    private String flat;
    private String flatLiter;

    public Address(Street street, String house, String houseLiter, String corpus, String flat, String flatLiter) {
        this.street = street;
        this.house = house;
        this.houseLiter = houseLiter;
        this.corpus = corpus;
        this.flat = flat;
        this.flatLiter = flatLiter;
    }



    public Street getStreet() {
        return street;
    }

    public String getHouse() {
        return house;
    }

    public String getHouseLiter() {
        return houseLiter;
    }

    public String getCorpus() {
        return corpus;
    }

    public String getFlat() {
        return flat;
    }

    public String getFlatLiter() {
        return flatLiter;
    }

    @Override
    public String toString(){
        if (street == null)
            return "";
        String res = street.getName() + ", "
                + house;
        if (!houseLiter.isEmpty())
            res += ", "
                    + houseLiter + ", "
                    + corpus + ", "
                    + flat;
        else
            res += ", "
                    + corpus + ", "
                    + flat;
        if (!flatLiter.isEmpty())
            res += ", "
                    + flatLiter;
        return res;
    }

    public boolean equals(Address address)
    {
        return street.equals(address.getStreet())&&
                house.equals(address.getHouse()) &&
                houseLiter.equals(address.getHouseLiter()) &&
                corpus.equals(address.getCorpus()) &&
                flat.equals(getFlat()) &&
                flatLiter.equals(address.getFlatLiter());
    }
}
