package exam.core;

import exam.CityN;

import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

public abstract class Data {

    public static LinkedHashMap enumSocStatus;
    public static LinkedHashMap enumLocale;
    public static LinkedHashMap enumDocType;

    public static CityN cityN;
    public static Set<Organization> organizations;
    public static Set<Region> regions;
    public static Set<Address> addresses;
    public static Set<Street> streets;

    public static void init(){
        cityN = new CityN();
        organizations = new HashSet<>(10000);
        regions = new HashSet<>(10000);
        addresses = new HashSet<>(10000);
        streets = new HashSet<>(10000);

        enumSocStatus = new LinkedHashMap<Integer, String>(){{
            put(1, "Работающий");
            put(2, "Дошкольник");
            put(3, "Учащийся");
            put(4, "Безработный");
            put(5, "Беженец");
            put(6, "Временно не работающий");
            put(7, "Пенсионер");
        }};

        enumLocale = new LinkedHashMap<Integer, String>(){{
            put(1, "Прописан по данному адресу");
            put(2, "Прописан по другому адресу");
            put(3, "Индивидуальный частный предприниматель");
        }};

        enumDocType = new LinkedHashMap<String, String>(){{
           put("1", "Паспорт");
           put("2", "Свидетельство о рождении");
           put("3", "Паспорт (работника МВД)");
           put("4", "Паспорт (иностранца)");
        }};
    }

    public static void clearTempLists() {
        regions = null;
        organizations = null;
        addresses = null;
        streets = null;
    }

    public static void addStreet(String line) {
        try {
            String[] tmp = line.split("\\t");
            streets.add(new Street(Integer.parseInt(tmp[0]), tmp[1]));
        }
        catch(IndexOutOfBoundsException e)
        {
            e.printStackTrace();
        }
    }

    public static void addRegion(String line) {
        String[] tmp = line.split("\\t");
        //String[] regionNumbers = tmp[0].split(" ");
        Region rg = new Region(Integer.parseInt(tmp[0]), Integer.parseInt(tmp[1]), Integer.parseInt(tmp[2]));
        rg.setName(tmp[3]);
        rg.setInsurer(Integer.parseInt(tmp[4]));
        rg.setLabel(Integer.parseInt(tmp[5]));
        regions.add(rg);
    }

    public static void addOrganisation(String line) {
        try {
            String[] strings = line.split("\\t");
            SimpleDateFormat format = new SimpleDateFormat("dd.mm.yyyy");
            Organization org = new Organization(Long.parseLong(strings[0]), Integer.parseInt(strings[1]), Integer.parseInt(strings[5]));
            org.setName(strings[4]);
            int t = 6;
            try {
                org.setDate(format.parse(strings[t]));
            } catch (Exception e) {
                System.out.println("Error with adding organisation");
            }
            t += 3;
            int rgn1 = Integer.parseInt(strings[t++]);
            int rgn2 = Integer.parseInt(strings[t++]);
            int rgn3 = Integer.parseInt(strings[t++]);
            Region rg = Data.findRegionById(rgn1, rgn2, rgn3);
            org.setRegion(rg);
            Street street = Data.findStreetById(Integer.parseInt(strings[t++]));
            Address addrr = new Address(street, strings[t], strings[t + 1], strings[t + 2], strings[t + 3], strings[t + 4]);
            org.setAddress(addrr);
            t += 5;
            org.setPhoneNumber(Integer.parseInt(strings[t++]));
            org.setFace(strings[t++]);
            org.setRf(Integer.parseInt(strings[t++]));
            org.setAdm(Integer.parseInt(strings[t++]));
            org.setBik(Integer.parseInt(strings[t++]));
            org.setBankName(strings[t++]);
            org.setOkpo(strings[t++]);
            org.setOkonh(strings[t]);
            organizations.add(org);
        } catch (Exception e) {
            //Ignore current
        }
    }

    public static Street findStreetById(Integer id){
        for (Street c: streets) {
            if (c.getId().equals(id)){
                return c;
            }
        }
        return null;
    }

    public static int citizenQty() {
        return cityN.getCitizenQty();
    }

    public static Region findRegionById(Integer rgn1, Integer rgn2, Integer rgn3){
        for (Region c:
             regions) {
            if (c.getRgn1() == rgn1 &&
                    c.getRgn2() == rgn2 &&
                    c.getRgn3() == rgn3)
                return c;
        }
        return null;
    }

    public static Organization findOrganizationByInn(Long inn){
        for (Organization c:
             organizations) {
            if (c.getInn().equals(inn))
                return c;
        }
        return null;
    }

    public static void setCityN(CityN newCityN){
        cityN = newCityN;
    }

}
