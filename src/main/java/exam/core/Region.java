package exam.core;

public class Region {

    private int rgn1;
    private int rgn2;
    private int rgn3;
    private String name;
    private int label;
    private int insurer;
    private int deleted;


    public Region() {
    }

    public Region(int rgn1, int rgn2, int rgn3) {
        this.rgn1 = rgn1;
        this.rgn2 = rgn2;
        this.rgn3 = rgn3;
    }

    public Region(int rgn1, int rgn2, int rgn3, String name, int label, int insurer, int deleted) {

        this.rgn1 = rgn1;
        this.rgn2 = rgn2;
        this.rgn3 = rgn3;
        this.name = name;
        this.label = label;
        this.insurer = insurer;
        this.deleted = deleted;
    }

    public int getRgn1() {
        return rgn1;
    }

    public int getRgn2() {
        return rgn2;
    }

    public int getRgn3() {
        return rgn3;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLabel() {
        return label;
    }

    public void setLabel(int label) {
        this.label = label;
    }

    public int getInsurer() {
        return insurer;
    }

    public void setInsurer(int insurer) {
        this.insurer = insurer;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }
	
	public boolean equals(Region rg)
	{
		if((rgn1 & rg.getRgn1()) == 0) return false;
		if((rgn2 & rg.getRgn2()) == 0) return false;
		if((rgn3 & rg.getRgn3()) == 0) return false;
		return true;		
	
	}
	
	public int getHashCode()
	{
		return rgn1^rgn2^rgn3;
	}

}
