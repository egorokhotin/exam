package exam.core;

import java.io.BufferedReader;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class Organization implements Serializable {

    private Long inn;
    private Integer insurer;
    private Integer argnum;
    private String name;
    private Address address;
    //private Street street;
    //private double insurant;
    //rivate int tfomsdep;
    private Date agrDate;
    private int numInsured;
    private int phone;
    private String face;
    private int rf;
    private int adm;
    private int bik;
    private String okpo;
    private String okonh;
    private int arg_ins;
    private String bankName;
    private Region region;


    public Organization(Long inn, Integer insurer, Integer argnum)
    {
        this.inn = inn;
        this.insurer = insurer;
        this.argnum = argnum;
        generateArgnumIns();
    }

    public void setFace(String face)
    {
        this.face = face;
    }

    public void setRf(int rf)
    {
        this.rf = rf;
    }

    public void setAdm(int adm)
    {
        this.adm = adm;
    }

    public void setBik(int bik)
    {
        this.bik = bik;
    }
    //public void setStreet(Street street)
    //{
    //    this.street = street;
    //}

    //public Street getStreet()
    //{
      //  return street;
    //}

    public void setAddress(Address address)
    {
        this.address = address;
    }

    public Address getAddress()
    {
        return address;
    }

    public void setPhoneNumber(Integer phone)
    {
        this.phone = phone;
    }

    public int getPhoneNumber()
    {
        return phone;
    }

    public void setOkpo(String okpo)
    {
        this.okpo = okpo;
    }

    public String getOkpo()
    {
        return okpo;
    }

    public void setOkonh(String okonh)
    {
        this.okonh = okonh;
    }

    public String getOkonh()
    {
        return okonh;
    }

    public String getBankName() throws Exception {
        if(bankName != null)
        {
            return bankName;
        }
        else throw new Exception();
    }

    public void setBankName(String name) {
            bankName = name;
    }

    private void generateArgnumIns()
    {
        arg_ins = argnum*1000 + insurer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setDate(Date date)
    {
        agrDate = date;
    }

    public Date getDate()
    {
        return agrDate;
    }

    public void setRegion(Region rg)
    {
        region = rg;
    }

    public Long getInn(){
        return inn;
    }

//    private String binarySearch(long key, BufferedReader reader)
//    {
//        String tmp;
//        String[] arr;
//        Long tmpLong;
//        tmp = reader.readLine();
//        int count, indx;
//        count = Integer.getInteger(tmp);
//        re
//    }
}
