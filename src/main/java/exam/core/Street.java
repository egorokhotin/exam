package exam.core;

import java.io.Serializable;

public class Street implements Serializable {

    private Integer id;
    private String name;

    public Street(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Street(Integer id)
    {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Street{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public boolean equals(Street street)
    {
        return id.equals(street.getId());
    }

    public int getHashCode()
    {
        return id;
    }
}
