package exam.core;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.StringTokenizer;

public class Citizen implements Serializable {

    private String id;//
    private Integer docNumber;//
    private Integer docNSeries;//
    private String docSeries;//
    private String  docType;
    private String  lastName;//
    private String  firstName;//
    private String  secondName;//
    private String  status;
    private LocalDateTime birthday;
    private String  sex;
    private String  policyNumber;//
    private String  policySeries;//
    private Region region;
    private Address address;
    private Integer locale;
    private Long tin;
    private Organization organization;
    private LocalDateTime  dateIn;//
    private LocalDateTime   changeDate;//

    public Citizen(String id, Integer docNumber, Integer docNSeries, String docSeries, String docType, String lastName, String firstName, String secondName, String status, String birthday, String sex, String policyNumber, String policySeries, Region region, Address address, Integer locale, Long tin, Organization organization, String dateIn, String changeDate) {
        this.id = id;
        this.docNumber = docNumber;
        this.docNSeries = docNSeries;
        this.docSeries = docSeries;
        this.docType = docType;
        this.lastName = lastName;
        this.firstName = firstName;
        this.secondName = secondName;
        this.status = status;
        this.birthday = parseToLocaleDateTime(birthday);
        this.sex = sex;
        this.policyNumber = policyNumber;
        this.policySeries = policySeries;
        this.region = region;
        this.address = address;
        this.locale = locale;
        this.tin = tin;
        this.organization = organization;
        this.dateIn = parseToLocaleDateTime(dateIn);
        this.changeDate = parseToLocaleDateTime(changeDate);
    }

    public Citizen()
    {

    }

    public String getId() {
        return id;
    }

    public Integer getDocNumber() {
        return docNumber;
    }

    public Integer getDocNSeries() {
        return docNSeries;
    }

    public String getDocSeries() {
        return docSeries;
    }

    public String getDocType() {
        return Data.enumDocType.get(docType).toString();
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getFullDocNumber(){
        return new StringBuilder().append(docNSeries).append(' ').append(docSeries).append(' ').append(docNumber).toString();
    }

    public String getFullPoliceNumber(){
        return new StringBuilder().append(policySeries).append(' ').append(policyNumber).toString();
    }

    public String getStatus() {
        if (status == null)
            return " ";
        return Data.enumSocStatus.get(Integer.parseInt(status)).toString();
    }

    public String getBirthDay() {
        return birthday.toLocalDate().toString();
    }

    public LocalDateTime LdtBirthday() {
        return birthday;
    }

    public String getSex() {
        if (Integer.parseInt(sex) == 1)
            return "М";
        else return "Ж";
    }

    public Region Region(){
        return region;
    }

    public String getRegion(){
        return region.getName();
    }

    public String getAddress() {
        return address.toString();
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public String getPolicySeries() {
        return policySeries;
    }

    public Long getTin() {
        return tin;
    }

    public String getOrganization() {
        if (organization == null)
            return "";
        return organization.getName();
    }

    public String getDateIn() {
        return dateIn.toLocalDate().toString();
    }

    public String getChangeDate() {
        return changeDate.toLocalDate().toString();
    }

    public String getLocale() {
        return Data.enumLocale.get(locale).toString();
    }

    public String getName()
    {
        return new StringBuilder().append(firstName).append(" ").append(secondName).append(" ").append(lastName).toString();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("\n\nCitizen{");
          sb.append("\nid=").append(id);
        sb.append(", \ndocNumber=").append(docNumber);
        sb.append(", \ndocNSeries=").append(docNSeries);
        sb.append(", \ndocSeries=").append(docSeries);
        sb.append(", \ndocType='").append(docType).append('\'');
        sb.append(", \nlastName='").append(lastName).append('\'');
        sb.append(", \nfirstName='").append(firstName).append('\'');
        sb.append(", \nsecondName='").append(secondName).append('\'');
        sb.append(", \nstatus='").append(status).append('\'');
        sb.append(", \nbirthday=").append(birthday);
        sb.append(", \nsex='").append(sex).append('\'');
        sb.append(", \naddress=").append(String.valueOf(address));
        sb.append(", \npolicyNumber='").append(policyNumber).append('\'');
        sb.append(", \npolicySeries='").append(policySeries).append('\'');
        sb.append(", \ntin=").append(tin);
        sb.append(", \norganization=").append(String.valueOf(organization));
        sb.append(", \ndateIn=").append(dateIn);
        sb.append(", \nchangeDate=").append(changeDate);
        sb.append('}');
        return sb.toString();
    }

    private LocalDateTime parseToLocaleDateTime(String s) {
        try {
            StringTokenizer tok = new StringTokenizer(s, "./");
            int day = Integer.parseInt(tok.nextToken());
            int month = Integer.parseInt(tok.nextToken());
            int year = Integer.parseInt(tok.nextToken());
            return LocalDateTime.of(year, month, day, 0, 0);
        }
        catch (Exception e){
            return LocalDateTime.MIN;
        }
    }


//region
    public void setId(String id)
    {
        this.id = id;
    }

    public void setDocNumber(Integer docNumber)
    {
        this.docNumber = docNumber;
    }

    public void setDocNSeries(Integer docNSeries)
    {
        this.docNSeries = docNSeries;
    }

    public void setDocSeries(String docSeries)
    {
        this.docSeries = docSeries;
    }

    public void setDocType(String docType)
    {
        this.docType = docType;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public void setSecondName(String secondName)
    {
        this.secondName = secondName;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public void setBirthDay(String birthday)
    {
        this.birthday = parseToLocaleDateTime(birthday);
    }

    public void setSex(String sex)
    {
        this.sex = sex;
    }

    public void setPolicyNumber(String policyNumber)
    {
        this.policyNumber = policyNumber;
    }

    public void setPolicySeries(String policySeries)
    {
        this.policySeries = policySeries;
    }

    public void setRegion(Region region)
    {
        this.region = region;
    }

    public void setAddress(Address address)
    {
        this.address = address;
    }

    public void setLocale(Integer locale)
    {
        this.locale = locale;
    }

    public void setTin(Long tin)
    {
        this.tin = tin;
    }

    public void setOrganization(Organization organization)
    {
        this.organization = organization;
    }

    public void setDateIn(String dateIn)
    {
        this.dateIn = parseToLocaleDateTime(dateIn);
    }

    public void setChangeDate(String changeDate)
    {
        this.changeDate = parseToLocaleDateTime(changeDate);
    }
//endregion сеты
}
